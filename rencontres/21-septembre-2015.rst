Rencontre
=========

21 septembre 2015

Présents :

- Adrien BÉRAUD
- Cyrille BÉRAUD
- Alexandre BLONDIN MASSÉ
- Simon DÉSAULNIERS
- Sylvain LABRANCHE
- Marco ROBADO

Discussion
==========

Cyrille:
  - Avancement récent de Ring
  - Meilleur flux pour les vidéos
  - Nouvelle version Mac OS sortira bientôt
  - Fonctionne bien sur Android pour des appels audio
  - Statistiques d'utilisation: 40% Windows, 10% Mac OS, 50% Linux

Adrien:
  - Réseau DHT: du point de vue utilisateur, c'est un ensemble de paires
    clé-valeur.
  - Service de relais

    * Technologie ICE: tente toutes les communications
    * IP à IP, cas idéal
    * Un IP, un NAT, fonctionne généralement
    * Dernier recours: double NAT, il faut utiliser un service relais
    * On souhaiterait indexer les noeuds de relais

  - Prioriser l'indexation de service, en particulier le service de relais
  - On verra après pour indexer d'autres informations
  - Chaque utilisateur, à terme, devrait pouvoir s'afficher comme serveur de
    relais
  - Protocole SIP: pour établir le contact téléphonique entre utilisateurs

Sylvain et Marco:
  - Question: comment le ID Ring est déterminé ?
  - Réponse :

    * Identifiant de routage : hash du noeud utilisé sur le réseau DHT
    * Identifiant Ring : identifiant de la paire de clés publique/privée
      (fingerprint)
    * Les deux sont indépendants

  - Service de relais

    * Préservation de l'anonymat ? Relais multiples
    * Performances ? Qualité de la connexion, proximité géographique, etc.
    * Connexion coupée, que faire ? Pas géré par le protocole TURN

Adrien:
  - Indexation des profils
  - Deux types d'utilisateurs :

    * Profil public : profil public sur le réseau (fiche de contact, v-card)
    * Profil privé : pas indexé, seulement pour les contacts

  - Présence ? Connecté, non disponible, non connecté, etc.
  - Plusieurs machines (devices), comment les gérer ? En construction. Une
    possibilité serait d'utiliser une clé-maître qui lierait tous les appareils
    (encrypté pour que seuls les contacts y aient accès).
  - Chaîne de certificats pour identification (je n'ai pas bien compris)
  - Stocker les profils (persistance de données)
  - Message hors ligne (en ce moment, seulement lorsque connecté)
  - Profil public : Nom, prénom, âge, ville, sexe, etc. (possiblement non
    spécifiés)
  - Type de données lors de put/get
  - Qui fait l'indexation ? L'utilisateur ou le réseau lui-même ?
  - Fautes de frappe ? Variation ? Solution simple indépendante de l'indexation
  - L'indexation devrait dépendre de la position géographique
