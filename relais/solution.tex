\documentclass[11pt,french]{article}

\usepackage{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[
backgroundcolor=green!40,
linecolor=black,
]{todonotes}

%%%%%%%%%%%%%%%%%
%  Page layout  %
%%%%%%%%%%%%%%%%%

\usepackage{geometry}
\setlength{\parindent}{0cm}
\setlength{\parskip}{1em}

\usepackage{fancyhdr}
\pagestyle{fancy}
\cfoot{}
\rfoot{\thepage}

% background image
\usepackage{eso-pic}
\newcommand\BackgroundPic{%
\put(0,0){%
\parbox[b][\paperheight]{\paperwidth}{%
\vfill
\centering
\includegraphics[width=\paperwidth,height=\paperheight,%
keepaspectratio]{background.png}%
\vfill
}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Math style paragraphs  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{amsmath}
\usepackage{amsthm}
\newtheorem{proposition}{Proposition}

\theoremstyle{remark}
\newtheorem{remark}{Remarque}

%%%%%%%%%%%%%%
%  Drawings  %
%%%%%%%%%%%%%%

\usepackage{float}
\usepackage{tikz}
\usepackage{pgf-umlsd}
\usepgflibrary{arrows}

\begin{document}
    \begin{titlepage}
        \title{Relais de communication pour Ring/OpenDHT}
        \author{Simon \textsc{Désaulniers}, Sylvain \textsc{Labranche}, Marco \textsc{Robado}}
        \maketitle
        \thispagestyle{empty}
    \end{titlepage}

    \AddToShipoutPicture{\BackgroundPic}
    \newgeometry{bottom=3cm}
    \section{Problème}
    \label{sec:probleme}

    \subsection{Description}
    \label{sub:description}

    Il s'agit d'indexer les n{\oe}uds offrant un service de relais. Les propositions suivantes
    caractérisent le service de \emph{recherche de relais}.

    \begin{proposition}[Non-persistence]
    \label{prop:non_persistence}
        L'offre d'un service de relais doit être en vigueur lorsque le nœud est en ligne. Celle-ci
        doit expirer lorsque le nœud quitte le réseau.
    \end{proposition}

    \begin{proposition}[Confidentialité]
    \label{prop:confidentialite}
        Un ou plusieurs relais ne doivent pas être privilégiés lors d'une requête de service. Le
        relais est obtenu de façon aléatoire.
    \end{proposition}

    \begin{proposition}[Scalabilité]
    \label{prop:scalabilite}
        Le service doit assurer un bon fonctionnement peu importe la grosseur du réseau.
    \end{proposition}

    \begin{remark}
        \label{rmk:sol_extensible}
        La solution du problème devrait pouvoir être extensible à l'indexation de tout type de
        service.
    \end{remark}

    \section{Solution}
    \label{sec:solution}

    \subsection{Rôles dans la « DHT »}
    \label{sub:roles_dans_la_dht_}

    Nous définissons d'abord trois rôles d'un n{\oe}ud OpenDHT, soient :
    \begin{itemize}
        \item {\bfseries Client}  : effectue des requêtes de données sur la DHT;
        \item {\bfseries Routeur} : maintient une table de routage des n{\oe}uds contactés;
        \item {\bfseries Serveur} : répond à des requêtes et stocke des données.
    \end{itemize}

    \subsection{Principe}
    \label{sub:principe}

    L'idée est d'avoir une deuxième DHT en parallèle qui indexe les fournisseurs de relais. On voit
    rapidement comment les propositions \ref{prop:confidentialite} et \ref{prop:scalabilite} sont
    respectés déjà puisqu'OpenDHT respecte déjà ces propositions par son fonctionnement interne ; il
    suffit de choisir un \emph{hash} aléatoire.

    Cette DHT est particulière puisqu'on y fait la distinction entre deux types de nœuds. En effet,
    un type de nœud jouerait le rôle de client et un autre de routeur et serveur. Appelons les
    respectivement \emph{client} et \emph{serveur}.

    \subsubsection{Serveur}
    \label{ssub:serveur}

    Un n{\oe}ud serveur offre le service de relais. Ce n{\oe}ud se comporte exactement à la manière
    d'un n{\oe}ud OpenDHT à l'exception qu'un service de relai suivant un quelconque protocole (TURN
    p. ex.) roule sur ce n{\oe}ud. La vérification de la disponibilité du service de relais sera
    cruciale au niveau du protocole de ce « sous-réseau ».

    \subsubsection{Client}
    \label{ssub:client}

    Le client se comporte de façon particulière. En effet :
    \begin{itemize}
        \item il est capable de faire des opérations {\ttfamily get()};
        \item il est furtif : il n'est pas contenu dans les tables de routage des autres n{\oe}uds;
        \item il garde aussi une table de routage, ce qui lui permet de trouver un autre relais plus
            rapidement et sans devoir passer par un n{\oe}ud « bootstrap » lors d'une recherche
            subséquente.
    \end{itemize}

    \begin{remark}
        Cette dichotomie du type de n{\oe}ud est suffisante afin d'assurer que le résultat d'une
        recherche soit bel et bien un n{\oe}ud de relais.
    \end{remark}

    \begin{remark}
        Soit $m$ clients et $n$ serveurs. Si chaque client fait une requête pour un relais, chaque
        routeur doit répondre à $\frac{m \log n}{n}$ requêtes en moyenne. Si $m$ est très grand par
        rapport à $n$, les n{\oe}uds relais pourraient être surchargés.
    \end{remark}

    \newpage
    \subsubsection{L'opération \emph{get}}
    \label{ssub:operation_get}

    Le diagramme de séquence suivant décrit la chaîne d'événements engendrée par l'opération
    {\ttfamily get()}.

    \begin{figure}[H]
        \centering
        \begin{sequencediagram}
            \newthread{c}{Client}
            \newinst[3]{s1}{75d5b}
            \newinst{s2}{58024}
            \newinst{s3}{32c94}

            \begin{call}{c}{findClosest(32c94)}{s1}{} \end{call}
            \begin{callself}{c}{addRT(75d5b)}{} \end{callself}

            \begin{call}{c}{findClosest(32c94)}{s2}{} \end{call}
            \begin{callself}{c}{addRT(58024)}{} \end{callself}

            \begin{call}{c}{findClosest(32c94)}{s3}{} \end{call}
            \begin{callself}{c}{addRT(32c94)}{} \end{callself}
        \end{sequencediagram}
        \caption{Diagramme de séquence : \emph{get} par client}
        \label{fig:diag-seq-get}
    \end{figure}

    \subsubsection{Connexion d'un « serveur »}
    \label{ssub:connexion_serveur}

    \begin{figure}[H]
        \centering
        \begin{sequencediagram}
            \newthread{s0}{602e4}
            \newinst[3]{s1}{75d5b}
            \newinst{s2}{32c94}
            \newinst{s3}{58024}

            \begin{call}{s0}{findClosest(602e4)}{s1}{}
                \begin{callself}{s1}{addRT(602e4)}{} \end{callself}
            \end{call}
            \begin{callself}{s0}{addRT(75d5b)}{} \end{callself}

            \begin{call}{s0}{findClosest(602e4)}{s2}{}
                \begin{callself}{s2}{addRT(602e4)}{} \end{callself}
            \end{call}
            \begin{callself}{s0}{addRT(32c94)}{} \end{callself}

            \begin{call}{s0}{findClosest(602e4)}{s3}{}
                \begin{callself}{s3}{addRT(602e4)}{} \end{callself}
            \end{call}
            \begin{callself}{s0}{addRT(58024)}{} \end{callself}
        \end{sequencediagram}
        \caption{Diagramme de séquence : connexion d'un serveur}
        \label{fig:diag-seq-connexion-serveur}
    \end{figure}

    \subsection{Protocole}
    \label{sub:protocole}
    Il serait nécessaire de modifier le protocole d'OpenDHT.

    Une \emph{première solution} simple serait d'ajouter la possibilité de faire une requête furtive
    : sans être ajouté à la table de routage du répondant. La courte présence des « clients » sur le
    réseau ne causerait pas de problèmes considérable de route car le temps nécessaire pour
    compléter une opération \emph{get} est négligeable.

    Une \emph{seconde solution} consisterait à épurer et/ou étendre le protocole d'OpenDHT seulement
    dans le « réseau de services ». En bref, il s'agirait d'OpenDHT sans capacité de stockage : un
    système de pige aléatoire distribué.

    \begin{remark}
        Dans les deux cas, le réseau de service est détaché du réseau OpenDHT usuel. Ensuite, il
        serait nécessaire qu'un nœud de service ne garde dans sa table de routage que les nœuds
        répondant de façon positive à un \emph{ping} associé au protocole du service.
    \end{remark}


    \subsection{Conception}
    \label{sub:conception}

    \todo[inline]{Préciser la solution.}
    %\begin{figure}[H]
       %\centering
       %\includegraphics[width=0.3\linewidth]{conception.pdf}
       %\caption{Conception}
       %\label{fig:conception}
    %\end{figure}

\end{document}
